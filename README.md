# Xitter UI

A small demo app replicating the front end to mastadon.

Requires node and elm. Build with `make`, test with `node server.js`

## Features
- written in elm
- can log in (see sever.js for email/password)
- can log out
- pages only a logged in person can see

## Todo
- Everything else
