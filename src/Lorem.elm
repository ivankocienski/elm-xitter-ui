module Lorem exposing (p1, p2, p3)


p1 : String
p1 =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin leo massa, euismod id dolor a, commodo tempor nisi. Donec interdum tellus et faucibus maximus. Pellentesque turpis mauris, dapibus euismod cursus at, vulputate vel mi. Mauris sed lacinia tortor. Nullam semper tempus orci dapibus semper. Cras vestibulum tortor et vehicula congue. Vivamus ullamcorper nunc quis rutrum imperdiet. In vitae diam ut lorem molestie imperdiet. Vestibulum fringilla libero ut risus semper vestibulum. Donec gravida eros non tellus vulputate lobortis. Fusce imperdiet tincidunt libero eget viverra. Aliquam tortor diam, accumsan vitae egestas non, condimentum eget nulla."


p2 : String
p2 =
    "Nulla molestie neque quis velit suscipit, ut volutpat orci lobortis. Sed viverra, orci non condimentum maximus, lacus neque convallis ipsum, eget posuere turpis lectus sit amet risus. Suspendisse orci justo, egestas ut purus et, tristique rhoncus justo. Aenean ac dapibus justo. Maecenas ut suscipit purus, quis ornare ipsum. Donec imperdiet, ligula id aliquet consectetur, nulla augue varius augue, quis pulvinar risus lacus ut arcu. Nullam a consectetur est, a tempus ligula. Praesent ut leo ipsum. Suspendisse in metus placerat, efficitur quam eget, luctus ligula. Nam vitae lobortis metus, in eleifend ante. Morbi sagittis felis nisl, sed malesuada diam lobortis ultrices. Cras aliquam luctus sapien ut vestibulum. Phasellus non lectus vulputate, porttitor sem quis, interdum felis. Praesent metus nibh, maximus id lacinia sed, euismod non libero. Morbi cursus lacus quis felis aliquet, ut iaculis felis pellentesque."


p3 : String
p3 =
    "Mauris dictum nunc elit, at faucibus leo tincidunt eu. Donec sed urna vehicula, varius enim et, laoreet massa. Integer sit amet commodo libero. Phasellus purus enim, malesuada a gravida ut, volutpat ut quam. Nulla congue vehicula nibh. Curabitur quis augue maximus, rhoncus dui et, cursus diam. Praesent viverra tortor est, a vehicula nunc pretium eu. Praesent aliquam quis ipsum at tristique. In vitae metus in diam lacinia venenatis sed vel diam. Maecenas luctus id neque nec convallis."
