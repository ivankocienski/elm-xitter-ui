module Views exposing (isLoggedIn, mustBeLoggedIn, viewPage)

--import Html exposing (Html, a, button, div, fieldset, footer, form, h1, input, label, li, nav, p, span, table, td, text, th, tr, ul)
--import Html.Attributes exposing ()

import Css exposing (Style, backgroundColor, block, bold, border3, color, display, displayFlex, em, fontSize, fontWeight, hex, hover, lineHeight, marginBottom, padding, pct, property, px, rem, solid, width)
import Html.Styled exposing (Html, a, button, div, fieldset, footer, form, h1, input, label, li, main_, nav, p, pre, span, table, td, text, textarea, th, tr, ul)
import Html.Styled.Attributes exposing (class, css, href, id, rows, type_, value)
import Html.Styled.Events exposing (onClick, onInput, onSubmit)
import Lorem
import Message exposing (..)
import Model exposing (FlashLevel(..), Model, Xeet)
import Routes exposing (..)
import Time



-- HELPERS


viewXeetList : List Xeet -> Html Msg
viewXeetList xeets =
    ul [ id "xeet-list" ]
        (List.map
            (\xeet ->
                li []
                    [ div [ class "body" ] [ text xeet.body ]
                    , p [ class "byline" ]
                        [ text "By "
                        , a [ href "#" ] [ text xeet.author ]
                        , text (" on " ++ xeet.datePosted)
                        ]
                    ]
            )
            xeets
        )


isLoggedIn : Model -> Bool
isLoggedIn model =
    case model.authToken of
        Just token ->
            String.length token > 0

        Nothing ->
            False


mustBeLoggedIn : Model -> (Model -> Html Msg) -> Html Msg
mustBeLoggedIn model onwardFunction =
    if isLoggedIn model then
        onwardFunction model

    else
        div [] [ text "You cannot access that page!" ]


mustNotBeLoggedIn : Model -> (Model -> Html Msg) -> Html Msg
mustNotBeLoggedIn model onwardFunction =
    if isLoggedIn model then
        div [] [ text "Cannot access this page when loggged in" ]

    else
        onwardFunction model


navbar : Model -> Html Msg
navbar model =
    let
        navLink path title route =
            if route == model.route then
                span [ css [ navLinkStyle, navActiveStyle ] ] [ text title ]

            else
                a [ href path, css [ navLinkStyle ] ] [ text title ]
    in
    nav [ css [ navStyle ] ]
        (if isLoggedIn model then
            let
                profileDisplayName =
                    case model.profile of
                        Just profile ->
                            profile.displayName

                        Nothing ->
                            "<not set>"
            in
            [ a [ href "/", css [ navLinkStyle, navBrandStyle ] ] [ text "Xitter" ]
            , navLink "/about" "About" AboutRoute
            , navLink "/compose" "Compose" ComposeXeetRoute
            , navLink "/profile" profileDisplayName ProfileRoute
            , button [ onClick TerminateSession ] [ text "Logout" ]
            ]

         else
            [ a [ href "/", css [ navLinkStyle, navBrandStyle ] ] [ text "Xitter" ]
            , navLink "/about" "About" AboutRoute
            , navLink "/login" "Login" LoginRoute
            ]
        )



-- VIEWS


viewHome : Model -> Html Msg
viewHome model =
    pre [ css [ debugBoxStyle ] ]
        [ text "Authentication status:\n\n"
        , text ("  token   : " ++ Maybe.withDefault "<not set>" model.authToken ++ "\n")
        ]


viewAbout : Model -> Html Msg
viewAbout _ =
    div [ css [ contentContainerStyle ] ]
        [ h1 [ css [ contentTitleStyle ] ] [ text "About" ]
        , p [ css [ contentParagraphStyle ] ] [ text Lorem.p1 ]
        , p [ css [ contentParagraphStyle ] ] [ text Lorem.p2 ]
        , p [ css [ contentParagraphStyle ] ] [ text Lorem.p3 ]
        ]


viewLogin : Model -> Html Msg
viewLogin model =
    mustNotBeLoggedIn model
        (\_ ->
            div []
                [ h1 [ css [ contentTitleStyle ] ] [ text "Log in" ]
                , form [ onSubmit LoginSubmit, css [ formStyle ] ]
                    [ fieldset [ css [ formFieldsetStyle ] ]
                        [ label [ css [ formLabelStyle ] ] [ text "Email" ]
                        , input [ type_ "text", value model.loginForm.email, onInput LoginEmail, css [ formTextFieldStyle ] ] []
                        ]
                    , fieldset [ css [ formFieldsetStyle ] ]
                        [ label [ css [ formLabelStyle ] ] [ text "Password" ]
                        , input [ type_ "password", value model.loginForm.password, onInput LoginPassword, css [ formTextFieldStyle ] ] []
                        ]
                    , fieldset [ css [ formFieldsetStyle ] ]
                        [ button [ css [ formButtonStyle ] ] [ text "Log in" ]
                        ]
                    ]
                ]
        )


viewProfile : Model -> Html Msg
viewProfile model =
    case model.profile of
        Just profile ->
            let
                tableRow title value =
                    tr []
                        [ th [] [ text title ]
                        , td [] [ text value ]
                        ]
            in
            mustBeLoggedIn model
                (\_ ->
                    div []
                        [ h1 [] [ text "Profile" ]
                        , p [] [ text "Here are your details" ]
                        , table []
                            [ tableRow "Display Name" profile.displayName
                            , tableRow "Email Address" profile.email
                            , tableRow "Joined" (String.fromInt (Time.posixToMillis profile.joined))
                            ]
                        ]
                )

        Nothing ->
            div [] [ text "Profile failed to load" ]


viewComposeXeet : Model -> Html Msg
viewComposeXeet model =
    mustBeLoggedIn model
        (\_ ->
            div []
                [ h1 [ css [ contentTitleStyle ] ] [ text "Compose Xeet" ]
                , form [ onSubmit ComposeXeetSubmit, css [ formStyle ] ]
                    [ fieldset [ css [ formFieldsetStyle ] ]
                        [ label [ css [ formLabelStyle ] ] [ text "Text" ]
                        , textarea [ onInput ComposeXeetText, rows 5, css [ formTextAreaStyle ] ]
                            [ text model.composeXeetForm.text ]
                        ]
                    , fieldset [ css [ formFieldsetStyle ] ]
                        [ button [ css [ formButtonStyle ] ] [ text "Post!" ]
                        ]
                    ]
                ]
        )


viewNotFound : Model -> Html Msg
viewNotFound _ =
    div [] [ text "not found" ]


viewFlash : Maybe FlashLevel -> Html Msg
viewFlash maybeFlash =
    case maybeFlash of
        Just flash ->
            case flash of
                FlashInfo message ->
                    div [ id "flash-info" ] [ text message ]

                FlashWarning message ->
                    div [ id "flash-warning" ] [ text message ]

                FlashError message ->
                    div [ id "flash-error" ] [ text message ]

        Nothing ->
            text ""


viewLayout : Model -> (Model -> Html Msg) -> Html Msg
viewLayout model viewFunction =
    div [ css [ bodyStyle ] ]
        [ navbar model
        , viewFlash model.flash
        , main_ [ css [ mainStyle ] ] [ viewFunction model ]
        , footer [] [ text "Copyright &copy; Poops" ]
        ]


viewPage : Model -> Html Msg
viewPage model =
    case model.route of
        HomeRoute ->
            viewLayout model viewHome

        AboutRoute ->
            viewLayout model viewAbout

        LoginRoute ->
            viewLayout model viewLogin

        ProfileRoute ->
            viewLayout model viewProfile

        ComposeXeetRoute ->
            viewLayout model viewComposeXeet

        NotFoundRoute ->
            viewLayout model viewNotFound



-- STYLES
-- TODO: make this into a Theme module


bodyStyle : Style
bodyStyle =
    Css.batch []


navStyle : Style
navStyle =
    Css.batch
        [ backgroundColor (hex "7cd1ce")
        , displayFlex
        , property "gap" "1rem"
        ]


navLinkStyle : Style
navLinkStyle =
    Css.batch
        [ padding (rem 0.9)
        , hover
            [ backgroundColor (hex "d5f1f0")
            ]
        ]


navBrandStyle : Style
navBrandStyle =
    Css.batch
        [ fontWeight bold
        ]


navActiveStyle : Style
navActiveStyle =
    Css.batch
        [ backgroundColor (hex "4d85ba")
        , color (hex "fff")
        ]


mainStyle : Style
mainStyle =
    Css.batch
        [ padding (rem 1)
        ]


debugBoxStyle : Style
debugBoxStyle =
    Css.batch
        [ border3 (px 1) solid (hex "555")
        , padding (em 1)
        , backgroundColor (hex "fcfcfc")
        ]


contentContainerStyle : Style
contentContainerStyle =
    Css.batch []


contentTitleStyle : Style
contentTitleStyle =
    Css.batch
        [ fontSize (rem 2.8)
        , fontWeight bold
        , marginBottom (rem 2)
        ]


contentParagraphStyle : Style
contentParagraphStyle =
    Css.batch
        [ lineHeight (em 1.7)
        , marginBottom (em 1)
        ]


formStyle : Style
formStyle =
    Css.batch []


formFieldsetStyle : Style
formFieldsetStyle =
    Css.batch [ marginBottom (rem 2) ]


formLabelStyle : Style
formLabelStyle =
    Css.batch
        [ display block
        , fontWeight bold
        , marginBottom (rem 0.5)
        ]


formTextFieldStyle : Style
formTextFieldStyle =
    Css.batch
        [ border3 (px 1) solid (hex "777")
        , padding (rem 1)
        , width (pct 40)
        ]


formTextAreaStyle : Style
formTextAreaStyle =
    Css.batch
        [ border3 (px 1) solid (hex "777")
        , padding (rem 1)
        , width (pct 40)
        ]


formButtonStyle : Style
formButtonStyle =
    Css.batch
        [ border3 (px 1) solid (hex "777")
        , padding (rem 1)
        , backgroundColor (hex "e0d2b1")
        ]
