port module Main exposing (main)

import Api exposing (getProfile, sendLogin)
import Browser exposing (application)
import Browser.Navigation as Nav
import Html.Styled
import Message exposing (Msg(..))
import Model exposing (ComposeXeetForm, FlashLevel(..), Model, Profile, emptyLoginForm, emptyModel, emptyXeetForm)
import Platform.Cmd as Cmd
import Routes exposing (Route(..), parseUrlToRoute)
import Time
import Url
import Views exposing (isLoggedIn, viewPage)



-- ports


port sessionToken : ( String, String ) -> Cmd msg


port recieveSessionToken : (String -> msg) -> Sub msg



-- code


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( emptyModel key (parseUrlToRoute url)
    , sessionToken ( "read", "" )
    )


triggerLoadForRoute : Model -> Cmd Msg
triggerLoadForRoute model =
    case model.authToken of
        Just authTokenString ->
            case model.route of
                ProfileRoute ->
                    getProfile authTokenString

                _ ->
                    Cmd.none

        Nothing ->
            Cmd.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        PageLinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            let
                newModel =
                    { model | route = parseUrlToRoute url }
            in
            ( newModel
            , triggerLoadForRoute newModel
            )

        LoginEmail emailString ->
            let
                modelForm =
                    model.loginForm
            in
            ( { model | loginForm = { modelForm | email = emailString } }
            , Cmd.none
            )

        LoginPassword passwordString ->
            let
                modelForm =
                    model.loginForm
            in
            ( { model | loginForm = { modelForm | password = passwordString } }
            , Cmd.none
            )

        LoginSubmit ->
            ( model, sendLogin model.loginForm )

        Authenticate result ->
            case result of
                Ok authResponse ->
                    if authResponse.status == "success" then
                        ( { model
                            | authToken = authResponse.sessionToken
                            , loginForm = emptyLoginForm
                            , flash = Just (FlashInfo "Welcome back")
                          }
                        , Cmd.batch
                            [ Nav.pushUrl model.key "/"
                            , sessionToken ( "set", Maybe.withDefault "" authResponse.sessionToken )
                            , getProfile (Maybe.withDefault "" authResponse.sessionToken)
                            ]
                        )

                    else
                        ( { model
                            | loginForm = emptyLoginForm
                            , flash = Just (FlashError "Log in failed")
                          }
                        , Cmd.none
                        )

                Err _ ->
                    ( { model
                        | loginForm = emptyLoginForm
                        , flash = Just (FlashError "Authentication error")
                      }
                    , Cmd.none
                    )

        TerminateSession ->
            let
                nextModel =
                    if isLoggedIn model then
                        { model
                            | authToken = Nothing
                            , flash = Just (FlashInfo "You have logged out")
                        }

                    else
                        model
            in
            ( nextModel
            , Cmd.batch
                [ Nav.pushUrl model.key "/"
                , sessionToken ( "clear", "" )
                ]
            )

        SetProfile result ->
            case result of
                Ok profileResponse ->
                    ( { model
                        | profile =
                            Just
                                (Profile
                                    profileResponse.emailAddress
                                    profileResponse.displayName
                                    (Time.millisToPosix profileResponse.joinTime)
                                )
                      }
                    , Cmd.none
                    )

                Err _ ->
                    ( model, sessionToken ( "clear", "" ) )

        RecieveSessionToken token ->
            ( { model | authToken = Just token }
            , getProfile token
            )

        ComposeXeetText value ->
            ( { model | composeXeetForm = ComposeXeetForm value }
            , Cmd.none
            )

        ComposeXeetSubmit ->
            -- TODO: post xeet to server, append to list of xeets
            ( { model | composeXeetForm = emptyXeetForm }
            , Cmd.none
            )


viewDocument : Model -> Browser.Document Msg
viewDocument model =
    { title = "App Demo"
    , body = [ viewPage model |> Html.Styled.toUnstyled ]
    }


subscriptions : Model -> Sub Msg
subscriptions _ =
    recieveSessionToken RecieveSessionToken


main : Program () Model Msg
main =
    application
        { init = init
        , update = update
        , view = viewDocument
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = PageLinkClicked
        }
