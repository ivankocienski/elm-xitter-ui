module Routes exposing (Route(..), parseUrlToRoute)

import Url
import Url.Parser as Parser exposing (Parser, oneOf)


type Route
    = HomeRoute
    | AboutRoute
    | LoginRoute
    | ProfileRoute
    | ComposeXeetRoute
    | NotFoundRoute


parseUrlToRoute : Url.Url -> Route
parseUrlToRoute url =
    Maybe.withDefault NotFoundRoute (Parser.parse routeParser <| url)


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ Parser.map HomeRoute Parser.top
        , Parser.map AboutRoute (Parser.s "about")
        , Parser.map LoginRoute (Parser.s "login")
        , Parser.map LoginRoute (Parser.s "logout")
        , Parser.map ProfileRoute (Parser.s "profile")
        , Parser.map ComposeXeetRoute (Parser.s "compose")
        ]
