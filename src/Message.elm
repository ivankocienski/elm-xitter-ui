module Message exposing (AuthenticationResponse, Msg(..), ProfileResponse)

import Browser exposing (UrlRequest, application)
import Browser.Navigation as Nav
import Http
import Url


type alias AuthenticationResponse =
    { status : String
    , sessionToken : Maybe String
    , message : Maybe String
    }


type alias ProfileResponse =
    { emailAddress : String
    , displayName : String
    , joinTime : Int
    }


type Msg
    = NoOp
    | UrlChanged Url.Url
    | PageLinkClicked Browser.UrlRequest
    | LoginEmail String
    | LoginPassword String
    | LoginSubmit
    | ComposeXeetText String
    | ComposeXeetSubmit
    | TerminateSession
    | Authenticate (Result Http.Error AuthenticationResponse)
    | SetProfile (Result Http.Error ProfileResponse)
    | RecieveSessionToken String
