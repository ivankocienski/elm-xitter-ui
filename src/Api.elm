module Api exposing (getProfile, sendLogin)

import Http
import Json.Decode exposing (Decoder, at, field, int, map3, maybe, string)
import Json.Encode as Encode
import Message exposing (AuthenticationResponse, Msg(..), ProfileResponse)
import Model exposing (LoginForm)
import Url.Builder as Url


httpErrorToString : Http.Error -> String
httpErrorToString error =
    case error of
        Http.BadUrl value ->
            "Bad URL: " ++ value

        Http.Timeout ->
            "Timeout"

        Http.NetworkError ->
            "Network Error"

        Http.BadStatus code ->
            "Bad status: " ++ String.fromInt code

        Http.BadBody message ->
            "Bad body: " ++ message


authenticationResponseDecoder : Decoder AuthenticationResponse
authenticationResponseDecoder =
    map3 AuthenticationResponse
        (field "status" string)
        (maybe (field "sessionToken" string))
        (maybe (field "message" string))


profileResponseDecoder : Decoder ProfileResponse
profileResponseDecoder =
    map3 ProfileResponse
        (at [ "profile", "email" ] string)
        (at [ "profile", "displayName" ] string)
        (at [ "profile", "joined" ] int)


sendLogin : LoginForm -> Cmd Msg
sendLogin form =
    let
        payload =
            Encode.object
                [ ( "email", Encode.string form.email )
                , ( "password", Encode.string form.password )
                ]
    in
    Http.post
        { url = "http://localhost:5000/api/v1/authenticate"
        , body = Http.jsonBody payload
        , expect = Http.expectJson Authenticate authenticationResponseDecoder
        }


getProfile : String -> Cmd Msg
getProfile token =
    if String.length token > 0 then
        let
            params =
                Url.toQuery [ Url.string "token" token ]
        in
        Http.get
            { url = "http://localhost:5000/api/v1/profile" ++ params
            , expect = Http.expectJson SetProfile profileResponseDecoder
            }

    else
        Cmd.none
