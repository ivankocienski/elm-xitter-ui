module Model exposing (ComposeXeetForm, FlashLevel(..), LoginForm, Model, Profile, Xeet, emptyLoginForm, emptyModel, emptyXeetForm)

import Browser.Navigation as Nav
import Routes exposing (Route)
import Time


type alias LoginForm =
    { email : String
    , password : String
    }


type alias ComposeXeetForm =
    { text : String
    }


type FlashLevel
    = FlashInfo String
    | FlashWarning String
    | FlashError String


type alias Profile =
    { email : String
    , displayName : String
    , joined : Time.Posix
    }


type alias Model =
    { key : Nav.Key
    , route : Route
    , loginForm : LoginForm
    , composeXeetForm : ComposeXeetForm
    , authToken : Maybe String
    , flash : Maybe FlashLevel
    , profile : Maybe Profile
    }


emptyModel : Nav.Key -> Route -> Model
emptyModel navKey route =
    Model
        navKey
        route
        emptyLoginForm
        emptyXeetForm
        Nothing
        Nothing
        Nothing


emptyLoginForm : LoginForm
emptyLoginForm =
    { email = ""
    , password = ""
    }


emptyXeetForm : ComposeXeetForm
emptyXeetForm =
    { text = ""
    }


type alias Xeet =
    { slug : String
    , author : String
    , body : String
    , datePosted : String
    }


sampleXeets : List Xeet
sampleXeets =
    [ { slug = "slug-1", author = "Alpha", body = "Text of xeet 1", datePosted = "10-20-3000" }
    , { slug = "slug-2", author = "Beta", body = "Text of xeet 2", datePosted = "09-20-3000" }
    , { slug = "slug-3", author = "Cappa", body = "Text of xeet 3", datePosted = "08-20-3000" }
    , { slug = "slug-4", author = "Delta", body = "Text of xeet 4", datePosted = "07-20-3000" }
    , { slug = "slug-5", author = "Epsilon", body = "Text of xeet 5", datePosted = "02-20-3000" }
    ]
