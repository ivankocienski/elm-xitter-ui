all:
	elm make src/Main.elm --output=public/app.js

format:
	elm-format --yes src/
