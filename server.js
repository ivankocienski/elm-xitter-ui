/* small local server for handling elm particularities */

const express = require('express');
const app = express();
const port = 5000;

const currentDirectory = process.cwd();

app.use(express.json());
app.use(express.static(currentDirectory + '/public/'));

function wrlog(msg) {
    console.log((new Date()).toISOString() + ": " + msg);
}

const SESSION_TOKEN = "1234567890abcdef";
const profile = {
    email: "user@example.com",
    displayName: "Spicey User",
    joined: Date.now()
};

function withValidAuthToken(req, res, thenDo) {
    let token = req.query.token;

    if(req.method == "POST") {
	token = req.body.token;
    }

    if(token == SESSION_TOKEN) {
	thenDo();
	return;
    }
	
    let payload = {
	status: "error",
	message: "Missing / bad session token"
    };

    res.status(401);
    res.send(payload);
}

app.get('/api/v1/profile', (req, res) => {
    wrlog("Reading profile");

    withValidAuthToken(req, res, () => {
	let payload = {
	    profile: profile
	};

	res.send(payload);
    });
});

app.post('/api/v1/authenticate', (req, res) => {
    wrlog("Authenticating");
    console.log(req.body);

    let payload = {
	status: "error",
	message: "Unknown email / password"
    };

    if(req.body.email == "user@example.com" && req.body.password == "password12345") {
	payload = {
	    status: "success",
	    sessionToken: SESSION_TOKEN
	};
    }

    res.send(payload);
});

app.get('/*', (req, res) => {
    wrlog("Sending index.html");

    res.sendFile(
	currentDirectory + "/public/index.html",
	{ headers: { 'Content-Type': 'text/html; charset=UTF-8' } }
    );
});

wrlog("Running in " + currentDirectory);

app.listen(port, () => {
    wrlog("Listenning on localhost:" + port);
});
